import os
import cv2
from PIL import Image
import numpy as np
# %pylab inline
import matplotlib.image as mpimg
from keras.preprocessing.image import ImageDataGenerator
import random
from random import randint

DATA_PATH = "data_label_container_samples/"
SAVE_PATH = "data_label_container_samples_convert/"

def image_gen(data_path, save_path):
  entries = os.listdir(data_path)
  images = []
  labels = []
  try:
    os.mkdir('data_label_container_samples_convert')
    os.mkdir('data_label_container_samples_convert/door')
    os.mkdir('data_label_container_samples_convert/no_door')
  except:
    print('Folder data_label_container_samples')
  entries_count = 0
  for entry in entries:
    if not entry.endswith('.txt') and str(entry[:-4] + ".txt") in entries:
      images.append(entry)
      labels.append(entry[:-4] + ".txt")
  for image, label in zip(images, labels):  
    file_name = image
    f = open(data_path + file_name[:-4] + ".txt", 'r')
    boxes = []
    line = f.readline()
    lines = []
    while line:
        lines.append(line)
        line = f.readline()

    boxes = [[float(x) for x in line.split(" ")] for line in lines]

    img = Image.open(data_path + file_name)
    W, H = img.size
    
    for box in boxes:
      x = int(box[1] * W)
      y = int(box[2] * H)
      w = int(box[3] * W)
      h = int(box[4] * H)
      crop_box = (x - w//2, y - h//2, x + w//2, y + h//2)
      crop_img = img.crop(crop_box)
      if(box[2] < 0.5):
        crop_img.save(save_path + "no_door/" + file_name)
      else:
        crop_img.save(save_path + "door/" + file_name)
    print('Solve ' + file_name)


image_gen(DATA_PATH, SAVE_PATH)