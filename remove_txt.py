import os

DATA_PATH = "data_label_container_samples/"

def remove_txt(data_path):
  entries = os.listdir(data_path)
  images = []
  labels = []
  for entry in entries:
    if not entry.endswith('.txt'): #and entry != "Gen" and randint(0, 4) == 0:
      images.append(entry)
      labels.append(entry[:-4] + ".txt")
  for image, label in zip(images, labels):  
    try:
        os.remove(data_path + label)
        print('Solve ' + label)
    except Exception as e:
        print(e)

remove_txt(DATA_PATH)